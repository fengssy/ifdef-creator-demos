// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

/// #if Web && Mobile
//注意，由于Creator的机制，在assets下的所有脚本都会被打包进来，所以本插件针对项目内本身模块的“选择是否打包功能”作用不大！
///// #code import { TestModuleInMobileWeb as TestModule } from "./TestModuleInMobileWeb";
/// #elif Web && Desktop
//预览用PC的版本
//import { TestModuleInPCWeb as TestModule } from "./TestModuleInPCWeb";
/// #endif

//尝试用插件“选择”外部模块（即npm模块），可以查看打包输出中是否包含不需要的外部模块代码
/// #if MiniApp
/// #code import { HttpClient, WsClient } from 'tsrpc-miniapp';
/// #else
import { HttpClient, WsClient } from 'tsrpc-browser';
/// #endif


@ccclass
export default class NewClass extends cc.Component {

    start () {
        //简单使用的条件编译

        /// #if Web
        console.log("web环境");
        /// #endif

        /// #if Mobile
        console.log("移动端环境");
        /// #elif Desktop
        console.log("桌面环境");
        /// #endif

        //组合条件表达式 和 条件嵌套
        var value:string;

        /// #if Web && Mobile
        ///     #code var testMsg2 = "H5-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("H5里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        console.log("H5里定义了SDK2");
        ///     #endif

        /// #elif Web && Desktop
        ///     #code var testMsg2="PC网页-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("PC网页里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        ///         #code console.log("PC网页里定义了SDK2,本条日志写在#code语句段中");
        ///     #endif
        /// #elif MiniApp
        ///     #code var testMsg2="小程序-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("小程序里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        ///         #code console.log("小程序里定义了SDK2,本条日志写在#code语句段中");
        ///     #endif
        /// #else
        var testMsg2 = "其他";
        /// #endif
        console.log(testMsg2);

        console.log("typeof(HttpClient):",typeof(HttpClient));

        //console.log("value:", TestModule.getValue());
    }
}
